import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriesOrderComponent } from './categories-order.component';

describe('CategoriesOrderComponent', () => {
  let component: CategoriesOrderComponent;
  let fixture: ComponentFixture<CategoriesOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriesOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
