export class Store {
  id: string;
  name: string;
  description: string;
  matrix: boolean;
  address: string;
  longitude: number;
  latitude: number;
  email: string;
  phone_number: string;
}
