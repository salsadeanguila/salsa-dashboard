export class Category {
  id: string;
  name: string;
  description: string;
  image_url: string;
  presigned_url: string;
  image: File;
  menu_order: number;
}
