import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule }    from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { PublicComponent } from './layouts/public.component';
import { SecureComponent } from './layouts/secure.component';
import { AppRoutes } from './app.routing';
import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';
import { FixedPluginModule} from './shared/fixedplugin/fixedplugin.module';
import { AgmCoreModule } from '@agm/core';

import { DashboardComponent }   from './secure/dashboard/dashboard.component';
import { CategoriesList } from './secure/categories/categories.component';
import { CategoryForm } from './secure/categories/category-form.component';
import { CategoriesOrder } from './secure/categories/categories-order.component';
import { ItemsList } from './secure/items/items.component';
import { ItemForm } from './secure/items/item-form.component';
import { StoresList } from './secure/stores/stores.component';
import { StoreForm } from './secure/stores/store-form.component';

import { LoginComponent } from './public/login/login.component';

import { AuthGuard } from './_guards/auth.guard';
import { AuthenticationService } from './_services/authentication.service';
import { DashboardService } from './_services/dashboard.service';
import { CategoriesService } from './_services/categories.service';
import { ItemsService } from './_services/items.service';
import { StoresService } from './_services/stores.service';
import { GeoService } from './_services/geoservice.service';



@NgModule({
  declarations: [
    PublicComponent,
    SecureComponent,
    DashboardComponent,
    LoginComponent,
    CategoriesList,
    CategoryForm,
    CategoriesOrder,
    ItemsList,
    ItemForm,
    StoresList,
    StoreForm
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(AppRoutes, { useHash: true }),
    SidebarModule,
    NavbarModule,
    FooterModule,
    FixedPluginModule,
    FormsModule,
    HttpModule,
    NgxDatatableModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD8kEMDXJoPqE5n5QuY1AUG7WNTvgmLYYw',
      language: 'spanish',
      region: 'MX'
    })
    // NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyBr-tgUtpm8cyjYVQDrjs8YpZH7zBNWPuY'})
  ],
  providers: [
    AuthGuard,
    AuthenticationService,
    GeoService,
    DashboardService,
    CategoriesService,
    ItemsService,
    StoresService
  ],
  bootstrap: [ PublicComponent ]
})
export class AppModule { }
