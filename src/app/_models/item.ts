export class Item {
  id: string;
  name: string;
  description: string;
  image_url: string;
  presigned_url: string;
  category_id: string;
  category_name: string;
  active: boolean = true;
  price: number;
  item_type: string;
}
