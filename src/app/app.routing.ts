import { Routes } from '@angular/router';

import { PublicComponent } from './layouts/public.component';
import { SecureComponent } from './layouts/secure.component';

import { PUBLIC_ROUTES } from './public/public.routes';
import { SECURE_ROUTES } from './secure/secure.routes';

import { AuthGuard } from './_guards/auth.guard';

export const AppRoutes: Routes = [
  {
    path: '', redirectTo: 'dashboard', pathMatch: 'full'
  },
  {
    path: '', component: PublicComponent, data: { title: 'Vistas públicas' }, children: PUBLIC_ROUTES
  },
  {
    path: '', component: SecureComponent, canActivate: [AuthGuard], data: { title: 'Vistas protegidas' }, children: SECURE_ROUTES
  }
];
