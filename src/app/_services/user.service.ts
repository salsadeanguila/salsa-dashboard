import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { AuthenticationService } from './authentication.service';
import { User } from '../_models/user';

import { environment } from '../../environments/environment';
const API_URL = environment.apiUrl;

@Injectable()
export class UserService {
  private userUrl =  API_URL + '/users';

  constructor(
      private http: Http,
      private authenticationService: AuthenticationService) {
  }

}
