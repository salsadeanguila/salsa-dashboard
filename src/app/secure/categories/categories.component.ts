import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { Category } from '../../_models/category';
import { CategoriesService } from '../../_services/categories.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesList implements OnInit {
  categories: Category[] = [];

  constructor(
    private categoriesService: CategoriesService,
    private router: Router) { }

  ngOnInit() {
    // Limpia el mode
    this.categoriesService.clearMode();
    // Lista de categorías
    this.categoriesService.getCategories()
        .subscribe(categories => {
          this.categories = categories;
        },
        err => {
          if(err.status == 401){
            this.router.navigate(['/login']);
          }
        }
      );
  }

  setMode(mode, category_id) {
    this.categoriesService.setMode(mode, category_id);
    this.router.navigate(['/categories/form']);
  }

}
