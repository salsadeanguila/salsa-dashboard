export const environment = {
  production: true,

  // URL de API de producción
  apiUrl: 'https://flordlerma-api.herokuapp.com'
};
