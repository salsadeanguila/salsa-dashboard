import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { AuthenticationService } from './authentication.service';

import { Category } from '../_models/category';
import { Item } from '../_models/item';

import { environment } from '../../environments/environment';
const API_URL = environment.apiUrl;

@Injectable()
export class DashboardService {
  public sidebarVisible: boolean;

  constructor(
    private http: Http,
    private authenticationService: AuthenticationService) {
      this.sidebarVisible = false;
  }

  getSidebarStatus(){
    return this.sidebarVisible;
  }

  changeSidebarStatus(){
    if (this.sidebarVisible == true) {
      this.sidebarVisible = false;
    } else {
      this.sidebarVisible = true;
    }
    return this.sidebarVisible;
  }

  getDashboardData(): Observable<any>{
    // Headers con token
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    let dashboardDataURL = API_URL + "/dashboard";

    return this.http.get(dashboardDataURL, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  private handleError(error: any){
    // if(error.status == 401){
    //   this.authenticationService.logout();
    // }
    return Observable.throw(error);
  }

}
