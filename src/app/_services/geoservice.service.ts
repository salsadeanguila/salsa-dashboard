import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
// import { MapsAPILoader } from '@agm/core';

declare var google: any;

@Injectable()
export class GeoService {

  constructor(
    private http: Http) {
  }

  createMap(element: any, props: any){
    return new google.maps.Map(element, props);
  }

  getCenter(lat: number, lng: number){
    return new google.maps.LatLng(lat, lng);
  }

  getLocation(address: string): Observable<any> {
    let geocodingURL = `https://maps.google.com/maps/api/geocode/json?address=${address}`;

    return this.http.get(geocodingURL)
      .map(this.extractData)
  }

  geocodeAddress(address: string, map: any){
    let geocoder = new google.maps.Geocoder();

    geocoder.geocode({'address': address}, (results, status) => {
      if (status === 'OK') {
        map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location
        });
      } else {
        console.log('Error: ' + status);
      }
    });
  }

  private extractData(response: Response) {
    let body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    return Observable.throw(error.json().error || 'Error del servidor');
  }

}
