import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { AuthenticationService } from './authentication.service';
import { Item } from '../_models/item';

import { environment } from '../../environments/environment';
const API_URL = environment.apiUrl;

@Injectable()
export class ItemsService {
  private allItemsURL = API_URL + '/items';
  private itemsURL = API_URL + '/categories/';
  private mode: string;
  private currentItemId: number;

  constructor(
    private http: Http,
    private authenticationService: AuthenticationService) {
  }

  clearMode(){
    localStorage.setItem('mode', '');
    localStorage.setItem('currentItemId', '');
    localStorage.setItem('currentItemCategory', '');
  }

  setMode(mode, id = 0, category = undefined){
    localStorage.setItem('mode', mode);
    if (id != 0) {
      localStorage.setItem('currentItemId', id.toString());
      localStorage.setItem('currentItemCategory', category.toString());
    }
  }

  getMode(){
    return localStorage.getItem('mode');
  }

  getCurrentItemId(){
    return localStorage.getItem('currentItemId');
  }

  getCurrentItemCategory(){
    return localStorage.getItem('currentItemCategory');
  }

  getItems(): Observable<Item[]> {
    // Headers con token
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });

    // Lista de items registrados
    return this.http.get(this.allItemsURL, options)
      .map((response: Response) => response.json());
  }

  createItem(item: Item): Observable<Item> {
    // Headers con token
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(item);
    let createItemURL = `${this.itemsURL}${item.category_id}/items`;

    return this.http.post(createItemURL, body, options)
      .map((response: Response) => response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Error del servidor'));
  }

  getItem(id: string, category: string): Observable<Item> {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    let getItemURL = `${this.itemsURL}${category}/items/${id}`;

    return this.http.get(getItemURL, options)
      .map((response: Response) => response.json());
  }

  updateItem(item: Item): Observable<Item> {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(item);
    let updateItemURL = `${this.itemsURL}${item.category_id}/items/${item.id}`;

    return this.http.put(updateItemURL, body, options)
      .map((response: Response) => response.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Error del servidor'));
  }

  deleteItem(id: string, category: string): Observable<boolean> {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    let deleteItemURL = `${this.itemsURL}${category}/items/${id}`;

    return this.http.delete(deleteItemURL, options)
      .map((response: Response) => {
        let eliminado = response.json() && response.json().eliminado;
        if (eliminado) {
          return true;
        } else {
          return false;
        }
      });
  }

  putFileToS3(body: File, presignedUrl: string) {
    let headers = new Headers({'Content-Type': 'image/jpeg'});
    let options = new RequestOptions({ headers: headers });

    return this.http.put(presignedUrl, body, options);
  }

}
