import { Component, OnInit } from '@angular/core';

import { NavbarComponent } from '../shared/navbar/navbar.component';

import { DashboardService } from '../_services/dashboard.service';

declare var $:any;

export interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Panel de control',  icon: 'ti-panel', class: '' },
    { path: '/categories', title: 'Categorías',  icon:'ti-tag', class: '' },
    { path: '/items', title: 'Ítems',  icon:'ti-shopping-cart', class: '' },
    { path: '/stores', title: 'Sucursales',  icon:'ti-direction', class: '' }
];

@Component({
    moduleId: module.id,
    selector: 'sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    private currentUser: any;

    constructor(public dashboardService: DashboardService){

    }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    }
    isNotMobileMenu(){
        if($(window).width() > 991){
            return false;
        }
        return true;
    }

    sidebarToggle(){
      var toggleButton = document.getElementsByClassName('navbar-toggle')[0];
      var body = document.getElementsByTagName('body')[0];

      if (this.dashboardService.getSidebarStatus() == true) {
        body.classList.remove("nav-open");
        toggleButton.classList.remove("toggled");
        this.dashboardService.changeSidebarStatus();
      }
    }

    hasClass( target, className ) {
      return new RegExp('(\\s|^)' + className + '(\\s|$)').test(target.className);
    }

}
