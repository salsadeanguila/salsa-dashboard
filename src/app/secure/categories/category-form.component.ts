import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { Category } from '../../_models/category';
import { CategoriesService } from '../../_services/categories.service';

import { Router } from '@angular/router';

import swal from 'sweetalert2';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CategoryForm implements OnInit {
  // "mode" indica si se está creando o editando una categoría
  mode: string;
  // loading para la animación
  loading = false;
  // categorías registradas
  categories: Category[] = [];
  // id de la categoría actual
  id: number;
  // objeto categoría de la categoría actual
  currentCategory: Category;
  // ruta de imagen para mostrar en la edición
  imgSrc: string;
  // archivo seleccionado para subir
  selectedFile: File;

  constructor(
    private router: Router,
    private categoriesService: CategoriesService) {

  }

  ngOnInit() {
    this.currentCategory = new Category;
    this.mode = this.categoriesService.getMode();

    if (this.mode == "editando") {
      // Se trae la categoría de la API usando la categoría actual
      // del category service
      this.getCategory(this.categoriesService.getCurrentCategoryId());

      // Limpia el ID por si no lo limpia antes
      // this.categoriesService.setCurrentCategoryId(0);
    }
    if (this.currentCategory.image_url == undefined) {
      this.imgSrc = "assets/img/angular.png";
    }
  }

  ngAfterContentInit() {
    // Este check es porque a veces no hace la petición de la categoría
    // Si se deja, haría doble petición de la categoría
    // if (this.currentCategory.name == undefined) {
    //   this.getCategory(this.categoriesService.getCurrentCategoryId());
    // }
  }

  editando(){
    if (this.mode == "editando") {
      return true;
    } else {
      return false;
    }
  }

  // Este mismo método se usará para crear y editar las categorías
  createCategory() {
    this.loading = true;

    // Revisa si existe un ID
    if (this.currentCategory.id) {
      // Está editando
      this.categoriesService.updateCategory(this.currentCategory)
        .subscribe(result => {
          // DEBUG
          // console.log(typeof result);
          // console.log(result);
          if (result.id) {
            if (result.presigned_url != undefined) {
              this.uploadToS3(result.presigned_url);
            }
            this.loading = false;
            swal({
              title: 'Categoría actualizada',
              type: 'success'
            });
            this.router.navigate(['/categories/']);
          } else {
            swal({
              title: 'Hubo un error al actualizar',
              type: 'error'
            });
          }

        });
    } else {
      // Está creando
      this.categoriesService.createCategory(this.currentCategory)
        .subscribe(result => {
          // DEBUG
          // console.log(typeof result);
          // console.log(result);

          // Si existe la propiedad ID en el resultado, se creó correctamente
          if (result.id) {
            if (result.presigned_url != undefined) {
              this.uploadToS3(result.presigned_url);
            }
            swal({
              title: 'Categoría creada',
              type: 'success'
            });
            this.router.navigate(['/categories/']);
          } else {
            this.loading = false;
            swal({
              title: 'Hubo un error al crear',
              type: 'error'
            });
          }
        });
    }
  }

  getCategory(id) {
    this.categoriesService.getCategory(id)
      .subscribe(result => {
        this.currentCategory.id = result.id;
        this.currentCategory.name = result.name;
        this.currentCategory.description = result.description;
        // this.currentCategory.image_url = result.image_url;
        if (result.image_url == undefined) {
          this.imgSrc = "assets/img/angular.png";
        } else {
          this.imgSrc = result.image_url + "#" + new Date().getTime();
        }
      },
        err => {
          if(err.status == 401){
            this.router.navigate(['/login']);
          }
        }
      );
  }

  deleteCategory(id){
    var thisLocal = this;
    swal({
      title: '¿Desea eliminar la categoría?',
      text: "Este cambio no puede deshacerse",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, quiero eliminarla',
      cancelButtonText: 'Cancelar'
    }).then(function () {
      thisLocal.categoriesService.deleteCategory(id)
        .subscribe(result => {
          if (result == true) {
            swal({
              title: 'Categoría eliminada',
              type: 'success'
            });
            thisLocal.router.navigate(['/categories/']);
          } else {
            swal({
              title: 'Hubo un error en el servidor',
              type: 'error'
            });
          }
        });
    })
    .catch(swal.noop);
  }

  fileEvent(fileInput: any){
    this.selectedFile = fileInput.target.files[0];
    this.currentCategory.image_url = this.selectedFile.name;
  }

  uploadToS3(presignedUrl: string) {
    this.categoriesService.putFileToS3(this.selectedFile, presignedUrl)
      .subscribe(
        response => console.log(response)
      );
  }

}
