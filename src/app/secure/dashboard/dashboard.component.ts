import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { DashboardService } from '../../_services/dashboard.service';

declare var $:any;

@Component({
    selector: 'dashboard-cmp',
    moduleId: module.id,
    templateUrl: 'dashboard.component.html'
})

export class DashboardComponent implements OnInit{
  public data: any;

  constructor(
    private dashboardService: DashboardService,
    private router: Router) {

  }

  ngOnInit(){
    this.dashboardService.getDashboardData()
      .subscribe(
        result => {
        this.data = result;
      },
        err => {
          if(err.status == 401){
            this.router.navigate(['/login']);
          }
        }
      );
  }
}
