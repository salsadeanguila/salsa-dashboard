import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { environment } from '../../environments/environment';
const API_URL = environment.apiUrl;

@Injectable()
export class AuthenticationService {
  public token: string;
  private loginUrl = API_URL + '/auth/login';

  constructor(private http: Http) {
    // Declara token en el estado actual si se encuentra un usuario en localStorage
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }

  login(email: string, password: string): Observable<boolean> {
    let user = { "email": email, "password": password };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let body = JSON.stringify(user);
    let options = new RequestOptions({ headers: headers });

    return this.http.post(this.loginUrl, body, options)
      .map((response: Response) => {
        // Inicio de sesión exitoso si se encuentra un token jwt en la respuesta
        let token = response.json() && response.json().auth_token;
        if(token){
          // Se guarda el token
          this.token = token;
          // Se guarda el correo y el token en localStorage para mantener al
          // usuario con su sesión iniciada entre páginas (que pueda navegar)

          let name = response.json().name;

          localStorage.setItem('currentUser', JSON.stringify({ email: email, name: name, token: token }));

          // DEBUG
          // console.log(localStorage.getItem('currentUser'));

          // Regresa true para indicar inicio de sesión correcto
          return true;
        } else {
          // Regresa false para indicar que falló el inicio de sesión
          return false;
        }
      });
  }

  logout(): void {
    // Limpia el token y remueve al usuario de localStorage para cerrar sesión
    this.token = null;
    localStorage.removeItem('currentUser');
  }

}
