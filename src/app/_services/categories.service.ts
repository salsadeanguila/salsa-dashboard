import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { AuthenticationService } from './authentication.service';
import { Category } from '../_models/category';

import { environment } from '../../environments/environment';
const API_URL = environment.apiUrl;

@Injectable()
export class CategoriesService {
  private categoriesURL = API_URL + '/categories';
  private mode: string;
  private currentCategoryId: number;

  constructor(
    private http: Http,
    private authenticationService: AuthenticationService) {
  }

  clearMode(){
    localStorage.setItem('mode', '');
    localStorage.setItem('currentCategoryId', '');
  }

  setMode(mode, id = 0){
    // this.mode = mode;
    // if (id != 0) {
    //   this.setCurrentCategoryId(id);
    // }
    localStorage.setItem('mode', mode);
    if (id != 0) {
        localStorage.setItem('currentCategoryId', id.toString());
    }
  }

  getMode(){
    // return this.mode;
    return localStorage.getItem('mode');
  }

  getCurrentCategoryId(){
    // return this.currentCategoryId;
    return localStorage.getItem('currentCategoryId');
  }

  setCurrentCategoryId(id){
    // this.currentCategoryId = id;
    localStorage.setItem('currentCategoryId', id.toString());
  }

  getCategories(): Observable<Category[]> {
    // Headers con token
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });

    // Lista de categorías registradas
    return this.http.get(this.categoriesURL, options)
      .map(this.extractData);
  }

  createCategory(category: Category): Observable<Category> {
    // Headers con token
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(category);

    return this.http.post(this.categoriesURL, body, options)
      .map(this.extractData);
  }

  getCategory(id: string): Observable<Category> {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    let categoryURL = `${this.categoriesURL}/${id}`;

    return this.http.get(categoryURL, options)
      .map(this.extractData);
  }

  updateCategory(category: Category): Observable<Category> {
    // Headers con token
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(category);
    let categoryURL = `${this.categoriesURL}/${category.id}`;

    return this.http.put(categoryURL, body, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  deleteCategory(id: string): Observable<boolean> {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    let categoryURL = `${this.categoriesURL}/${id}`;

    return this.http.delete(categoryURL, options)
      .map((response: Response) => {
        let eliminado = response.json() && response.json().eliminado;
        if (eliminado) {
          return true;
        } else {
          return false;
        }
      });
  }

  putFileToS3(body: File, presignedUrl: string) {
    let headers = new Headers({'Content-Type': 'image/jpeg'});
    let options = new RequestOptions({ headers: headers });

    return this.http.put(presignedUrl, body, options);
      // DEBUG
      // .map((response: Response) => console.log(response));
  }

  private extractData(response: Response) {
    let body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    return Observable.throw(error.json().error || 'Error del servidor');
  }

}
