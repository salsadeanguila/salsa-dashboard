import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { Category } from '../../_models/category';
import { CategoriesService } from '../../_services/categories.service';

import swal from 'sweetalert2';

@Component({
  selector: 'app-categories-order',
  templateUrl: './categories-order.component.html',
  styleUrls: ['./categories-order.component.css']
})
export class CategoriesOrder implements OnInit {
  loading: boolean = false;
  categories: Category[] = [];

  constructor(
    private router: Router,
    private categoriesService: CategoriesService) {
  }

  ngOnInit() {
    this.getCategories();
  }

  getCategories(){
    this.categoriesService.getCategories()
      .subscribe(categories => {
        this.categories = categories;
      },
      err => {
        if(err.status == 401){
          this.router.navigate(['/login']);
        }
      });
  }

  saveOrder(){
    this.loading = true;
    let inputs = document.getElementsByClassName('input-categoria');
    let confirmaciones = new Array();

    for (let i = 0; i < inputs.length; i++) {
      let category: Category = new Category();
      category.id = inputs[i]["id"];
      category.menu_order = inputs[i]["value"];

      this.categoriesService.updateCategory(category)
        .subscribe(result => {
          if (result) {
            confirmaciones.push(result);
            if (i === inputs.length - 1) {
              this.loading = false;
              swal({
                title: 'Categorías actualizadas',
                type: 'success',
                text: `Se actualizaron ${confirmaciones.length} de un total de ${this.categories.length} categorías.`
              });
              this.router.navigate(['/categories/']);
            }
          }
        },
        err => {
          if(err.status == 401){
            this.router.navigate(['/login']);
          }
        });
    }
  }

}
