import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../../_services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: any = {};
  loading = false;
  error = '';

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this.authenticationService.logout();
  }

  login() {
    this.loading = true;

    // DEBUG
    // console.log(this.user.email);
    // console.log(this.user.password);

    this.authenticationService.login(this.user.email, this.user.password)
      .subscribe(result => {
        if (result === true) {
            this.router.navigate(['dashboard']);
        } else {
          this.error = "Correo o contraseña incorrecta. Por favor, vuelva a intentarlo.";
          this.loading = false;
          // console.log("Error");
        }
      });
  }

}
