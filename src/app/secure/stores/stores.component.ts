import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { Store } from '../../_models/store';
import { StoresService } from '../../_services/stores.service';

@Component({
  selector: 'app-stores',
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.css']
})
export class StoresList implements OnInit {
  stores: Store[] = [];

  constructor(
    private storesService: StoresService,
    private router: Router) {
  }

  ngOnInit() {
    this.storesService.clearMode();

    this.storesService.getStores()
      .subscribe(stores => {
        this.stores = stores;
      },
      err => {
        if (err.status == 401) {
          this.router.navigate(['/login']);
        }
      });
  }

  setMode(mode, store_id) {
    this.storesService.setMode(mode, store_id);
    this.router.navigate(['/stores/form']);
  }

  isMatrix(matrix){
    if (matrix == true) {
      return true;
    } else {
      return false;
    }
  }

}
