import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

import { AuthenticationService } from './authentication.service';
import { Store } from '../_models/store';

import { environment } from '../../environments/environment';
const API_URL = environment.apiUrl;

@Injectable()
export class StoresService {
  private storesURL = API_URL + '/stores';
  private currentStoreId: number;

  constructor(
    private http: Http,
    private authenticationService: AuthenticationService) {
  }

  clearMode(){
    localStorage.setItem('mode', '');
    localStorage.setItem('currentStoreId', '');
  }

  setMode(mode, id = 0){
    localStorage.setItem('mode', mode);
    if (id != 0){
      localStorage.setItem('currentStoreId', id.toString());
    }
  }

  getMode(){
    return localStorage.getItem('mode');
  }

  getCurrentStoreId(){
    return localStorage.getItem('currentStoreId');
  }

  setCurrentStoreId(id){
    localStorage.setItem('currentStoreId', id.toString());
  }

  getStores(): Observable<Store[]> {
    // Headers con token
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });

    return this.http.get(this.storesURL, options)
      .map(this.extractData);
  }

  createStore(store: Store): Observable<Store> {
    // Headers con token
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(store);

    return this.http.post(this.storesURL, body, options)
      .map(this.extractData);
  }

  getStore(id: string): Observable<Store> {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    let storeURL = `${this.storesURL}/${id}`;

    return this.http.get(storeURL, options)
      .map(this.extractData);
  }

  updateStore(store: Store): Observable<Store> {
    // Headers con token
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(store);
    let storeURL = `${this.storesURL}/${store.id}`;

    return this.http.put(storeURL, body, options)
      .map(this.extractData);
  }

  deleteStore(id: string): Observable<boolean> {
    // Headers con token
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    let storeURL = `${this.storesURL}/${id}`;

    return this.http.delete(storeURL, options)
      .map((response: Response) => {
        let eliminado = response.json() && response.json().eliminado;
        if (eliminado) {
          return true;
        } else {
          return false;
        }
      });
  }


  private extractData(response: Response) {
    let body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    return Observable.throw(error.json().error || 'Error del servidor');
  }

}
