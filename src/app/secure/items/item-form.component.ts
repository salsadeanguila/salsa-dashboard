import { Component, OnInit } from '@angular/core';

import { Item } from '../../_models/item';
import { Category } from '../../_models/category';
import { ItemsService } from '../../_services/items.service';
import { CategoriesService } from '../../_services/categories.service';

import { Router } from '@angular/router';

import swal from 'sweetalert2';

@Component({
  selector: 'app-item-form',
  templateUrl: './item-form.component.html',
  styleUrls: ['./item-form.component.css']
})
export class ItemForm implements OnInit {
  // "mode" indica si se está creando o editando una categoría
  mode: string;
  // loading para la animación
  loading = false;
  // id de la categoría actual
  id: number;
  // ruta de imagen para mostrar en la edición
  imgSrc: string;
  // archivo seleccionado para subir
  selectedFile: File;
  // Objeto Item actual
  currentItem: Item;
  // Catálogo de categorías para dropdown menu
  categories: Category[] = [];

  constructor(
    private router: Router,
    private itemsService: ItemsService,
    private categoriesService: CategoriesService
  ) { }

  ngOnInit() {
    // Inicializa ítem
    this.currentItem = new Item;

    // Trae categorías existentes
    this.categoriesService.getCategories()
      .subscribe(categories => {
        this.categories = categories;
      },
        err => {
          if(err.status == 401){
            this.router.navigate(['/login']);
          }
        }
      );
    // Registra si estamos creando o editando
    this.mode = this.itemsService.getMode();

    if (this.mode == "editando") {
      this.getItem(
        this.itemsService.getCurrentItemId(),
        this.itemsService.getCurrentItemCategory()
      );
    }

  }

  ngAfterContentInit() {
    if (this.currentItem.image_url == undefined) {
      this.imgSrc = "assets/img/angular.png";
    } else {
      this.imgSrc = this.currentItem.image_url;
    }
  }

  editando(){
    if (this.mode == "editando") {
      return true;
    } else {
      return false;
    }
  }

  saveItem(model: Item, isValid: boolean) {
    // Valida si el formulario se llenó correctamente
    if (isValid) {
      this.loading = true;
      if (this.currentItem.id) {
        // Está editando
        this.itemsService.updateItem(this.currentItem)
          .subscribe(result => {
            if (result.id) {
              if (result.presigned_url != undefined) {
                this.uploadToS3(result.presigned_url);
              }
              this.loading = false;
              swal({
                title: 'Ítem actualizado',
                type: 'success'
              });
              this.router.navigate(['/items/']);
            } else {
              swal({
                title: 'Hubo un error al actualizar',
                type: 'error'
              });
            }
          },
          err => {
            if(err.status == 401){
              this.router.navigate(['/login']);
            }
          }
        );
      } else {
        // Está creando
        this.itemsService.createItem(this.currentItem)
          .subscribe(result => {
            if (result.id) {
              if (result.presigned_url != undefined) {
                this.uploadToS3(result.presigned_url);
              }
              swal({
                title: 'Ítem creado',
                type: 'success'
              });
              this.router.navigate(['/items/']);
            } else {
              this.loading = false;
              swal({
                title: 'Hubo un error al crear',
                type: 'error'
              });
            }
          },
          err => {
            if(err.status == 401){
              this.router.navigate(['/login']);
            }
          }
        );
      }
    } else {
      swal({
        title: 'Error',
        text: 'Faltan campos por llenar',
        type: 'warning'
      });
    }
  }

  getItem(id, category) {
    this.itemsService.getItem(id, category)
      .subscribe(result => {
        if (result) {
          this.currentItem = result;
          if (result.image_url == undefined) {
            this.imgSrc = "assets/img/angular.png";
          } else {
            this.imgSrc = result.image_url + "#" + new Date().getTime();
          }
        } else {
          alert("Error al traer el ítem");
        }
      },
      err => {
        if(err.status == 401){
          this.router.navigate(['/login']);
        }
      }
    );
  }

  deleteItem(id, category){
    swal({
      title: '¿Desea eliminar el ítem?',
      text: "Este cambio no puede deshacerse",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, quiero eliminarlo',
      cancelButtonText: 'Cancelar'
    }).then(() => {
      this.itemsService.deleteItem(id, category)
        .subscribe(result => {
          if (result == true) {
            swal({
              title: 'Ítem eliminado',
              type: 'success'
            });
            this.router.navigate(['/items/']);
          } else {
            swal({
              title: 'Hubo un error en el servidor',
              type: 'error'
            });
          }
        },
        err => {
          if(err.status == 401){
            this.router.navigate(['/login']);
          }
        }
      );
    }).catch(swal.noop);
  }

  fileEvent(fileInput: any){
    this.selectedFile = fileInput.target.files[0];
    this.currentItem.image_url = this.selectedFile.name;
  }

  uploadToS3(presignedUrl: string) {
    this.itemsService.putFileToS3(this.selectedFile, presignedUrl)
      .subscribe(
        response => console.log(response)
      );
  }

  // debug(){
  //   console.log("debugea");
  // }

}
