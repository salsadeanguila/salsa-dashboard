import { Component, OnInit } from '@angular/core';

import { Store } from '../../_models/store';
import { StoresService } from '../../_services/stores.service';
import { GeoService } from '../../_services/geoservice.service';

import { Router } from '@angular/router';

import swal from 'sweetalert2';

@Component({
  selector: 'app-store-form',
  templateUrl: './store-form.component.html',
  styleUrls: ['./store-form.component.css']
})
export class StoreForm implements OnInit {
  // "mode" indica si se está creando o editando una sucursal
  mode: string;
  // loading para la animación
  loading = false;
  // sucursales registradas
  matrix: Store;
  // id de la sucursal actual
  id: number;
  // objeto categoría de la categoría actual
  currentStore: Store;
  // Coordenadas generales de Tampico para inicializar
  lat: number = 22.250278;
  lng: number = -97.864610;
  // Zoom decente para apreciar el marcador y la ubicación
  zoom: number = 14;
  // Flag para desplegar texto de mapa
  noAddressFound: boolean = false;

  constructor(
    private router: Router,
    private storesService: StoresService,
    private geoservice: GeoService) {

  }

  ngOnInit() {
    // Inicializa objeto Store (independiente si se crea o edita)
    this.currentStore = new Store;
    // Obtiene el modo en que se está llamando el formulario (creando/editando)
    this.mode = this.storesService.getMode();
    // Trae las tiendas registradas para saber cuál es la matriz
    this.storesService.getStores()
      .subscribe(stores => {
        for(let store of stores){
          if (store.matrix == true) {
            this.matrix = store;
            break;
          }
        }
      },
      err => {
        if(err.status == 401){
          this.router.navigate(['/login']);
        }
      }
    );
    // Si se está editando, se trae el objeto Store de la base de datos
    if (this.mode == "editando") {
      this.getStore(this.storesService.getCurrentStoreId());
    } else {
      // Si no está editando, se le ponen las coordenadas por default
      this.currentStore.latitude = this.lat;
      this.currentStore.longitude = this.lng;
    }

  }

  editando(){
    if (this.mode == "editando") {
      return true;
    } else {
      return false;
    }
  }

  validateStoreForm(model: Store, isValid: boolean) {
    if (isValid) {
      // Formulario válido
      this.loading = true;
      if (this.matrix == undefined) {
        this.saveStore();
        return false;
      }
      if (this.matrix.id == this.currentStore.id) {
        // Está editando la matríz, actualizar sin problema
        this.saveStore();
      } else {
        // No está editando la matríz actual
        if (this.currentStore.matrix == true) {
          // Si señaló como matríz a la tienda actual, preguntar si desea cambiar
          swal({
            title: 'Ya existe una tienda matríz',
            text: `Está intentando guardar esta sucursal (${this.currentStore.name}) como matríz. La tienda matríz actual es: ${this.matrix.name}, ¿Desea hacer el cambio?`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, quiero cambiarla',
            cancelButtonText: 'Cancelar'
          }).then(() => {
            // Si confirma el cambio, actualizar esta nueva y quitar la pasada

            // Actualiza la sucursal que se está editando
            this.saveStore();

            // Actualiza la tienda matríz anterior para que deje de serlo
            this.matrix.matrix = false;

            this.storesService.updateStore(this.matrix)
              .subscribe(result => {},
                err => {
                  if(err.status == 401){
                    this.router.navigate(['/login']);
                  }
                  this.loading = false;
                }
              );
          })
          .catch(swal.noop);
          // Si no, regresa a la pantalla y permitirá desactivar la casilla para poder guardar
          this.loading = false;
        } else {
          // No está editando la matríz y no hará matríz a la sucursal que se está editando
          this.saveStore();
        }
      }

    } else {
      // Formulario no válido
      console.log("Formulario no válido.");
    }
  }

  saveStore(){
    if (this.currentStore.id) {
      // Está editando
      this.storesService.updateStore(this.currentStore)
        .subscribe(result => {
          if (result.id) {
            this.loading = false;
            swal({
              title: 'Sucursal actualizada',
              type: 'success'
            });
            this.router.navigate(['/stores']);
          } else {
            swal({
              title: 'Hubo un error al actualizar',
              type: 'error'
            });
          }
        },
        err => {
          if(err.status == 401){
            this.router.navigate(['/login']);
          }
          this.loading = false;
        }
      );
    } else {
      // Está creando
      this.storesService.createStore(this.currentStore)
        .subscribe(result => {
          if (result.id) {
            swal({
              title: 'Sucursal creada',
              type: 'success'
            });
            this.router.navigate(['/stores']);
          } else {
            swal({
              title: 'Hubo un error al crear',
              type: 'error'
            });
          }
        },
        err => {
          if (err.status == 401) {
            this.router.navigate(['/stores']);
          }
          this.loading = false;
        }
      );
    }
  }

  getStore(id) {
    this.storesService.getStore(id)
      .subscribe(result => {
        // Se trae los datos de la tienda
        this.currentStore = result;
        // Pone las coordenadas de la tienda en el marcador
        this.lat = this.currentStore.latitude;
        this.lng = this.currentStore.longitude;
        // Centra el mapa en el marcador


      },
      err => {
        if (err.status == 401) {
          this.router.navigate(['/login']);
        }
      }
    );
  }

  getCoordFromAddress(address: string){
    if (address == "") {
      // Si se dejo de hacer focus en el campo pero no hay texto, se pone el default
      this.currentStore.latitude = this.lat;
      this.currentStore.longitude = this.lng;
    } else {
      this.geoservice.getLocation(address)
        .subscribe(result => {
          if (result.status == 'OK') {
            // Cambia coordenadas del currentStore
            this.currentStore.latitude = result.results[0].geometry.location.lat;
            this.currentStore.longitude = result.results[0].geometry.location.lng;
            this.noAddressFound = false;
          } else {
            this.noAddressFound = true;
          }
        });
    }
  }

  changeMarkerPos(event){
    // Cambia coordenadas del mapa y marcador
    this.lat = event.coords.lat;
    this.lng = event.coords.lng;
    // Coordenadas del currentStore
    this.currentStore.latitude = event.coords.lat;
    this.currentStore.longitude = event.coords.lng;
  }

  deleteStore(id) {
    if (this.currentStore.matrix == true) {
      // No puede eliminar la tienda matriz
      swal({
        title: 'No puede eliminar la tienda principal',
        text: 'Debe marcar como tienda principal otra de las sucursales para poder eliminar esta.',
        type: 'warning'
      });
    } else {
      swal({
        title: '¿Desea eliminar la sucursal?',
        text: "Este cambio no puede deshacerse",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, quiero eliminarla',
        cancelButtonText: 'Cancelar'
      }).then(() => {
        this.storesService.deleteStore(id)
          .subscribe(result => {
            if (result == true) {
              swal({
                title: 'Sucursal eliminada',
                type: 'success'
              });
              this.router.navigate(['/stores']);
            } else {
              swal({
                title: 'Hubo un error en el servidor',
                type: 'error'
              });
            }
          },
          err => {
            if(err.status == 401){
              this.router.navigate(['/login']);
            }
          }
        );
      })
      .catch(swal.noop);
    }
  }

}
