import { Routes } from '@angular/router';

import { DashboardComponent }   from './dashboard/dashboard.component';
import { CategoriesList }   from './categories/categories.component';
import { CategoryForm } from './categories/category-form.component';
import { CategoriesOrder } from './categories/categories-order.component';
import { ItemsList } from './items/items.component';
import { ItemForm } from './items/item-form.component';
import { StoresList } from './stores/stores.component';
import { StoreForm } from './stores/store-form.component';

export const SECURE_ROUTES: Routes = [
  {
      path: 'dashboard',
      component: DashboardComponent
  },
  {
      path: 'categories',
      component: CategoriesList
  },
  {
      path: 'categories/form',
      component: CategoryForm
  },
  {
      path: 'categories/order',
      component: CategoriesOrder
  },
  {
      path: 'items',
      component: ItemsList
  },
  {
      path: 'items/form',
      component: ItemForm
  },
  {
      path: 'stores',
      component: StoresList
  },
  {
      path: 'stores/form',
      component: StoreForm
  }
];
