import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router) { }

  canActivate() {
    // DEBUG
    // console.log(localStorage);
    if(localStorage.getItem('currentUser')){
      // sesión iniciada
      return true;
    }

    // sin sesión
    this.router.navigate(['/login']);
    return false;
  }
}
