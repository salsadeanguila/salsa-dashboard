import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs/Rx';
import { Observable } from 'rxjs';

import { Router } from '@angular/router';

import { Item } from '../../_models/item';
import { ItemsService } from '../../_services/items.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ItemsList implements OnInit {
  // dtOptions: DataTables.Settings = {};
  items: Item[] = [];

  rows = [];
  columns = [
    { prop: 'id' },
    { prop: 'name' },
    { name: 'category_name' },
    { prop: 'price' },
    { prop: 'active' },
    { prop: 'edit' }
  ];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;

  constructor(
    private itemsService: ItemsService,
    private router: Router) {
  }

  ngOnInit() {
    this.itemsService.clearMode();

    this.itemsService.getItems()
      .subscribe(items => {
        this.items = items;
        this.rows = this.items;
        this.loadingIndicator = false;
      },
        err => {
          if(err.status == 401){
            this.router.navigate(['/login']);
          }
        }
      );
  }

  buscarItem(event) {
    const val = event.target.value.toLowerCase();

    const temp = this.items.filter(function(d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rows = temp;
  }

  setMode(mode, item_id){
    let category_id: string;
    if(item_id == undefined){
      category_id = undefined;
    } else {
      category_id = document.getElementById(item_id).getAttribute("data-category-id");
    }
    this.itemsService.setMode(mode, item_id, category_id);
  }


}
